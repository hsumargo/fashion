<?php
class ValHelper {
	
	public static $match_event = array('' => '', 'Lineup' => 'Lineup', 'General' => 'General', 'Kick Off' => 'Kick Off', 'Half Time' => 'Half Time', 'Full Time' => 'Full Time',
			'Second Half' => 'Second Half', 
			'First Extra Time' => 'First Extra Time', 'Second Extra Time' => 'Second Extra Time', 'Foul' => 'Foul', 
			'Throw In' => 'Throw In', 'Corner Kick' => 'Corner Kick', 'Free Kick' => 'Free Kick', 'Penalty Kick' => 'Penalty Kick', 'Own Goal' => 'Own Goal', 
			'Goal Kick' => 'Goal Kick', 'Yellow Card' => 'Yellow Card', 'Red Card' => 'Red Card', 'Yellow Red Card' => 'Yellow Red Card', 'Injury' => 'Injury', 'Offside' => 'Offside', 'Goal' => 'Goal', 'Substitution' => 'Substitution',
			'Shot' => 'Shot', 'Shot On Target' => 'Shot On Target', 'referee' => 'Referee', 'spectators' => 'Spectators', 
			'man_of_the_match' => 'Man of The Match');
	
	public static $positions = array('' => '', 'GK' => 'GK', 'DF' => 'DF', 'MF' => 'MF', 'CF' => 'CF');
	
	public static $filter_players = array('first_name' => 'First Name', 'last_name' => 'Last Name', 'nick_name' => 'Nick Name',
			'shirt_number' => 'Shirt Number', 'position' => 'Position');
	
	public static $league = array('1' => 'UK', '2' => 'SPN', '3' => 'ITA', '4' => 'GER', '5' => 'NED', '6' => 'FRA');
	
	public static $special_league = array('7' => 'uefa_champ_league', '8' => 'europa_league');
	//manually update every new league year
	public static $standings_index = array('1' => 1, '2' => 21, '3' => 41, '4' => 61, '5' => 79, '6' => 97);

	public static $default_league_id = 1;
	
	public static $default_special_league_id = 7;
	
	/**
	 * @var  Swiftmailer  Holds Swiftmailer instance
	 */
	protected static $_mail;
	
	public static $email = 'support@soccerticker.net';
	
	public static function errs($val) {
		$errors = array();
		
		foreach($val->error() as $err) {
			$errors[] = $err->get_message();
		}
		
		return $errors;
	}
	
	public static function pr() {
		$vars = func_get_args();
		if(count($vars) > 0) {
			echo '<pre>',PHP_EOL;
			foreach($vars as $v) {
				echo print_r($v, 1), PHP_EOL;
			}
			echo '</pre>',PHP_EOL;
		}
	}
	
	public static function find_file($dir, $file, $ext = null) {
		if ($ext === NULL) {
			$ext = '.php';
		} elseif ($ext) {
			$ext = ".{$ext}";
		} else {
			$ext = '';
		}
		
		$path = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR .
				$dir . DIRECTORY_SEPARATOR . $file . $ext;
		
		return $path;
	}
	
	public static function fb_config() {
		return array(
			'app_id' => '347728311990505',
			'secret' => 'bf9750893291f6e1184f7e62f5b963b2',
			'base_url' => Uri::create('api/facebook_login')
		);
	}
	
	public static function tw_config() {
		return array(
			'key' => 'BDtXZewcLwx3fTB0TOvvag',
			'secret' => 'j4ZforEluzH0ACBTlOyEs3kh6Rub2aZIPP27C5MTk',
			'base_url' => Uri::create('admin/twitter_login'),
			'oauth_token' => '65214436-WU9IzKo4XKSwzrrl7iswIKpyJyoa80LwnZPOpaC5U',
			'oauth_token_secret' => 'SxaTovqZGYWyCXIsSeByH1ixc4JP2DqguKUF5rzhWY'
		);
	}
	
	public static function rm_strlower($str) {
		return strtolower(str_replace(' ', '', $str));
	}
	
	public static function sort($array) {
		usort($array, function($a, $b) {
			return $a - $b;
		});
		return $array;
	}
	
	public static function email($from, $to, $subject, $body) {
		// Create an instance
		$email = Email::forge();
		
		// Set the from address
		$email->from($from);
		
		// Set a subject
		$email->subject($subject);
		
		// Set multiple to addresses
		
		$email->to($to);
		
		// And set the body.
		$email->html_body($body);
		
		return $email->send();
	}
	
	public static function swiftmail_send($header = null, $from = array(), $to = array(), $body, $bcc = array()) {
		$transport = Swift_SmtpTransport::newInstance('localhost', 25);
		
		//5 seconds timeout
		$transport->setTimeout(5);
		
		// Create the Mailer using your created Transport
		$mailer = Swift_Mailer::newInstance($transport);
		
		//docs: http://swiftmailer.org/docs/plugins.html
// 		$mailer->registerPlugin(new Swift_Plugins_AntiFloodPlugin(1, 60)); //don't flood email
		
		// Create a message
		$message = Swift_Message::newInstance($header)
		->setFrom($from)
		->setTo($to)
		->setBcc($bcc)
		->setBody($body);
		$message->setContentType('text/html');
		
		// Send the message
		$result = $mailer->send($message);
		
		return $result;
	}
}
