<div id="canvas-wrapper" class="canvas-wrapper">
    <canvas id="line1" class="canvas-line" width="2000" height="1000"></canvas>
    <div class="canvaslink">
        <a href="#" class="link1" data-id="aboutus"><span>ABOUT</span></a>
        <a href="#" class="link2" data-id="collection"><span>COLLECTION</span></a>
        <a href="#" class="link3" data-id="contact"><span>CONTACT</span></a>
        <a href="#" class="link4" data-id="press"><span>PRESS</span></a>
    </div>
</div>
<!-- SPLASH SCREEN START -->
<div id="logo2" class="logofix"></div>
<div id="logo" class="logo">
    <div class="logo-arrow"></div>
    <div class="logo-bow"></div>
    <div class="logo-leftwing"></div>
    <div class="logo-rightwing"></div>
    <div class="logo-diamond"></div>
    <div class="logo-diamond-shine"></div>
</div>
<div id="logo3" class="logofix2"></div>
<!-- SPLASH SCREEN END -->

<!-- -->
<div id="aboutus" style="display:none;">
    <div class="banner">
        <ul>
            <li>
                <div class="content-wrapper nano" align="center">
                    <div class="content">
                        <?php 
                            if(!empty($about_photo)):
                                echo Html::img('assets/uploads/' . $about_photo, array('alt' => '', 'width' => 360, 'height' => 220)); 
                            endif;
                            if(!empty($about_description)):
                                echo nl2br($about_description);
                            endif;
                        ?>
                        
                    </div>
                </div>
                <div class="title-bar">ABOUT US</div>
            </li>
            <li>
                <div class="content-wrapper nano" align="center">
                    <div class="content">
                        <?php 
                        if(!empty($about_achievement)):
                             echo nl2br($about_achievement); 
                        endif; 
                        ?>

                    </div>
                </div>
                <div class="title-bar">ACHIEVEMENT</div>
            </li>
        </ul>
    </div>
</div>

<div id="collection" style="display:none;">
    <div class="content-wrapper nano">
        <div class="content">
            <div class="collection-menu">
                <ul>
                    <?php 
                    if(!empty($categories)):
                        foreach($categories as $i => $c): ?>
                            <li><a data-id="<?php echo $c; ?>" href="#"><?php echo strtoupper($c); ?></a></li>
                    <?php endforeach;
                    endif; 
                    ?>
                    
                </ul>
            </div>
        </div>
    </div>
    <div class="title-bar">COLLECTIONS</div>
</div>

<?php foreach($collections as $i => $co): ?>
    <div id="collection-<?php echo $i; ?>" class="co-details" style="display:none;">
        <div class="banner">
            <ul>
                <?php foreach($co as $idx => $item):?>
                    <li><img src="../../../../assets/uploads/<?php echo $item['filename']; ?>" class="img-slide" /></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>    
<?php endforeach; ?>

<div id="press" class="collection-detail" style="display:none;">
    <div class="banner">
        <ul>
            <?php foreach($press as $p): ?>
                <li><img src="../../../../assets/uploads/<?php echo $p['filename']; ?>" class="img-slide" /></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<div id="contact" style="display:none;">
    <div class="banner">
        <ul>
            <li>
                <div class="content-wrapper nano">
                    <div class="content">
                        
                            <p>
                                <?php echo $contact_address; ?><br />
                                <?php echo $contact_city; ?><br />
                                <?php echo $contact_country; ?><br />
                                P. <?php echo $contact_phone1; ?><br />
                                <?php echo $contact_phone2; ?><br />
                                <?php echo $contact_phone3; ?><br />
                                F. <?php echo $contact_fax; ?><br /><br />
                                E. <a href="mailto:<?php echo $contact_email; ?>"><?php echo $contact_email; ?></a><br />
                                <a href="<?php echo $contact_website; ?>"><?php echo $contact_website; ?></a>
                            </p>
                        
                        <div class="contact-link">
                            <a href="<?php echo $fb_link; ?>" target="_blank" class="icon-facebook"></a>
                            <a href="<?php echo $tw_link; ?>" target="_blank" class="icon-twitter"></a>
                            <a href="<?php echo $insta_link; ?>" target="_blank" class="icon-instagram"></a>
                            <a href="<?php echo $youtube_link; ?>" target="_blank" class="icon-youtube"></a>
                        </div>
                    </div>
                </div>
                <div class="title-bar">CONTACT</div>
            </li>
            <li>
                <div class="content-wrapper nano">
                    <div class="content" align="center">

                        <?php foreach($links as $i => $link): ?>
                            <p><?php echo $link['name'] ?>: <br /><a href="http://<?php echo $link['link']; ?>" target="_blank"><?php echo $link['link']; ?></a></p>
                        <?php endforeach; ?>

                    </div>
                </div>
                <div class="title-bar">LINK</div>
            </li>
        </ul>
    </div>
</div>