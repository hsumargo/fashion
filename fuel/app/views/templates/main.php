<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=800">
	<title>RINALDY A. YUNARDI</title>
	<?php echo $css; ?>
</head>
<body>

	<?php if(!empty($admin)): ?>
	<div class="container" style="margin-top:35px;">
		<div class="span12">
			<div class="box"  style="<?php if(!Auth::check()) echo 'background:whiteSmoke; -webkit-box-shadow:0 0 0 0; box-shadow:0 0 0 0'?>">
			<?php if(!empty($success)): ?>
			<div class="alert alert-success"><?php echo $success ?></div>
			<?php elseif(!empty($errors)): ?>
			<div class="alert alert-error">
				<?php foreach($errors as $err): ?>
					<div><?php echo $err ?></div>
				<?php endforeach; ?>
			</div>
			<?php endif; ?>
			<?php echo $main_view; ?>
			</div>
		</div>
	</div>
	<?php else: ?>
		<?php echo $main_view; ?>
	<?php endif; ?>
	<?php echo $js; ?>
</body>
</html>
