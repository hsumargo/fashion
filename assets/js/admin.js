$(document).ready(function() {
	$(".tablesorter").tablesorter();
	// When page loads...
	$(".tab_content").hide(); // Hide all content
	$("ul.tabs li:first").addClass("active").show(); // Activate first tab
	$(".tab_content:first").show(); // Show first tab content

	// On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); // Remove any "active" class
		$(this).addClass("active"); // Add "active" class to selected tab
		$(".tab_content").hide(); // Hide all tab content

		var activeTab = $(this).find("a").attr("href"); // Find the href
														// attribute value to
														// identify the active
														// tab + content
		$(activeTab).fadeIn(); // Fade in the active ID content
		return false;
	});
	
//	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
	tinymce.init({
	    selector: "textarea",
	    plugins: [
	        "advlist autolink lists link image charmap print preview anchor",
	        "searchreplace visualblocks code fullscreen",
	        "insertdatetime media table contextmenu paste"
	    ],
	    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | fontselect fontsizeselect",
		fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt"
	});
	$('.btn-danger').click(function(e) {
		var sure = confirm("Are you sure want to delete this?");
		if (sure == true){
			// alert('Successfully Deleted');
			location.reload();
		} else {
			e.preventDefault();
			return false;
		}
	});
	
});
$(function() {
	$('.column').equalHeight();
});