<article class="module width_full">
	<header>
		<h3 class="tabs_involved">Press Content Manager</h3>
	</header>

	<div class="tab_container">
		<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0">
				<thead>
					<tr>
						<th>Press ID</th>
						<th>Name</th>
						<th>Created On</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($press as $p):?>
						<tr>
							<td><?php echo $p['id']; ?></td>
							<td><?php echo ucfirst($p['name']); ?></td>
							<td><?php echo (!empty($p['created_at'])) ? date('d M Y', $p['created_at']) : ''; ?></td>
							<td>
								<small> <?php echo Html::anchor(Uri::create('admin/edit_press/' . $p['id']), '<i class="icon-edit"></i> Edit Press', array('class' => 'btn btn-mini')); ?></small>
								<small> <?php echo Html::anchor(Uri::create('admin/delete/press/' . $p['id']), '<i class="icon-trash"></i> Delete Press', array('class' => 'btn btn-mini btn-danger')); ?></small>
							</td>
						</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		</div>