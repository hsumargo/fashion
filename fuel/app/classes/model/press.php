<?php 
class Model_Press extends Model_Base {
	static $_properties = array(
		'id', 
		'name' => array('label' => 'Name'),
		'category_id' => array('label' => 'Category ID'),
		'filename' => array('label' => 'Filename'),
		'description' => array('label' => 'Description'),
		'deleted' => array('default' => 0),
		'created_at', 'updated_at', 'created_by', 'updated_by'
	);

	/** Add a press
	 * @param for required parameter, see $_properties
	 */
	public function add_press() {
		$val = Validation::forge();
		$val->add_model($this);

		if($val->run()) {
			if($this->save()) {
				return $this->id;
			} else {
				return false;
			}
		} else {
			return $val->error();
		}
	}
	
	/** Edit a press
	 * @param for required parameter, see $_properties
	 */
	public function edit_press($data) {
		// $val = Validation::instance();
		// $val->add_model($this);
		
		// if($val->run()) {
			return $this->set($data)->save();
		// } else {
		// 	return $val->error();
		// }
	}
	
	/** get all press
	 * @param
	 */
	public function get_press() {
		$query = DB::select('*')
				->from('presses')
				->order_by('created_at')
				->where('presses.deleted', '0')
				->execute()->as_array();

		$results = array();
		
		return $query;
	}
	
}