<?php
class Controller_Base extends \Controller
{
	
	public $template = 'templates/main';
	
	public $admin_template = 'templates/admin';
	private $default_control = 'welcome';
	public $data = array(
		// contains <style>'s to put into the view
		'css' => null,
	
		// contains <script>'s to put into the view
		'js' => null,
		'main' => array()
	);
	
	//Javascript variable to be set in the view
	public $js_vars = array();
	
	//Include jquery ui?
	public $jquery_ui = false;
	
	public $nav = true;
	/**
	 * Load the template and create the $this->template object
	 */
	public function before()
	{	
		$this->data['nav'] = $this->nav;
		return parent::before();
	}

	/**
	 * After controller method has run output the template
	 *
	 * @param  Response  $response
	 */
	public function after($response)
	{
		if (!empty($response))
		{
			return $response;
		}

		Casset::js('jquery-1.9.1.min.js');
		
		if(!empty($this->data['admin'])) {
			Casset::css('bootstrap.min.css');
			Casset::css('bootstrap-responsive.min.css');
			Casset::css('admin.css');
			Casset::css('layout.css');
			Casset::js('bootstrap.min.js');
			Casset::js('hideshow.js');
			Casset::js('jquery.tablesorter.min.js');
			Casset::js('jquery.equalHeight.js');
			Casset::js('tinymce.min.js');
			Casset::js('admin.js');
		} else {
			Casset::css('default.css');
			Casset::js('default.js');
			Casset::js('unslider.min.js');
			Casset::js('jquery.nanoscroller.min.js');
			

			// Casset::js('jquery.simplemodal.1.4.4.min.js');
			// if($this->request->action == 'index') {
			// 	Casset::js('tinyfader-packed.js');
			// 	Casset::js('main.js');
			// }
			// if($this->request->action == 'collection') {
			// 	Casset::js('tinyfader-packed.js');
			// 	Casset::js('main.js');
			// }
			// Casset::js('ss.js');
			// Casset::js('public.js');
			// Casset::js('jquery.nanoscroller.min.js');
		}
		
		$this->specific_assets('css');
		$this->specific_assets('js');
		
		$this->data['css'] = Casset::render_css();
		$this->data['js'] = Casset::render_js();
		$this->js_vars['base_uri'] = Uri::base();
		
		if(!empty($this->data['subviews'])) {
			foreach($this->data['subviews'] as $subview => $fpath) {
				$this->data['subviews'][$subview] = View::forge($fpath, $this->data);
			}
		}
		//External js variables
		$this->data['js_vars'] = sprintf("<script>var js_vars = %s</script>", json_encode($this->js_vars));
		
		$this->data['main_view'] = View::forge($this->main_view, $this->data);
		//jQuery UI
		$this->data['jquery_ui'] = $this->jquery_ui;
		
		$response = (strtolower(substr($this->request->route->controller, 11)) == $this->default_control) ? View::forge($this->template, $this->data) : View::forge($this->admin_template, $this->data);
		return parent::after($response);
	}
	
	//Find controller and view specific asset
	protected function specific_assets($ext) {
		//View's
		$asset = $this->main_view . '.' . $ext;
		
		//Combine with controller's name
		$file = 'extra/' . $asset;
		
		//Search in css/ or js/ depending on $ext
		if(Asset::find_file($file, $ext) !== false) {
			Casset::$ext($file);
		}
	}
	
	//Cache convenience
	protected function cache_set($name, $value) {
		Cache::set($name, $value);
	}
	
	//Set $default if CacheNotFoundException is thrown
	protected function cache_get($name) {
		$value = null;
		try {
			$value = Cache::get($name);
		} catch (CacheNotFoundException $e) {
			return false;
		}
		
		return $value;
	}

	/**
	 * Returns an array of options for Form::select()
	 * @param array $data
	 * @param string $key key to extract from $data
	 * @param mixed $value value to extract from $data
	 * @param boolean $empty true to include empty selection
	 */
	protected function make_list($data, $key, $value, $empty = true) {
		$options = array();
		
		if($empty) $options[] = null;
		
		foreach($data as $datum) {
			if(is_array($value)) {
				foreach($value as $v) {
					$options[$datum[$key]][] = $datum[$v];
				}
			} else {
				$options[$datum[$key]] = $datum[$value];
			}
		}
		
		return $options;
	}

}
