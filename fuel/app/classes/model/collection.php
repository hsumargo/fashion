<?php 
class Model_Collection extends Model_Base {
	static $_properties = array(
		'id', 
		'name' => array('label' => 'Name'),
		'category_id' => array('label' => 'Category ID', 'validation' => array('required')),
		'filename' => array('label' => 'Filename'),
		'description' => array('label' => 'Description'),
		'deleted' => array('default' => 0),
		'created_at', 'updated_at', 'created_by', 'updated_by'
	);

	/** Add a collection
	 * @param for required parameter, see $_properties
	 */
	public function add_collection() {
		$val = Validation::forge();
		$val->add_model($this);

		if($val->run()) {
			if($this->save()) {
				return $this->id;
			} else {
				return false;
			}
		} else {
			return $val->error();
		}
	}
	
	/** Edit a collection
	 * @param for required parameter, see $_properties
	 */
	public function edit_collection($data) {
		$val = Validation::instance();
		$val->add_model($this);
		
		if($val->run()) {
			return $this->set($data)->save();
		} else {
			return $val->error();
		}
	}
	
	/** get all collections
	 * @param
	 */
	public function get_collections($admin = false) {
		$query = DB::select(DB::expr('collections.id, collections.name, collections.description, 
				collections.filename, collections.category_id, collections.created_at, categories.name as "category_name"'))
				->from('collections')
				->join('categories', 'left')
				->on('categories.id', '=', 'collections.category_id')
				->order_by('collections.category_id')
				->where('collections.deleted', '0')
				->execute()->as_array();

		$results = array();
		
		if($admin === false) {
			foreach ($query as $i => $item) {
				$results[$item['category_name']][] = $item;
			}	
		} else {
			return $query;
		}
		
		return $results;
	}
	
}