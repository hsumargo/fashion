<?php
use Orm\Model;
class Model_Base extends Model {
	public $errors = array();
	
    protected static $_observers = array(
        'Orm\\Observer_CreatedAt', 'Orm\\Observer_UpdatedAt'
    );	
}
