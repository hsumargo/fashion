<?php
/**
 * The Ajax Controller.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Ajax extends Controller_Base
{

	/**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{	
	}

	/**
	 * @access  public
	 * @return  Response
	 */
	public function action_get_collection()
	{
		if(Input::is_ajax()) {

		}
		die;
	}

	/**
	 * The 404 action for the application.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		return Response::forge(ViewModel::forge('welcome/404'), 404);
	}
}
