<?php 
class Model_Link extends Model_Base {
	static $_properties = array(
		'id', 
		'name' => array('label' => 'Name'),
		'link' => array('label' => 'Link', 'validation' => array('required')),
		'is_show' => array('label' => 'Show', 'validation' => array('required')),
	);

	/** Add a link
	 * @param for required parameter, see $_properties
	 */
	public function add_link() {
		$val = Validation::forge();
		$val->add_model($this);

		if($val->run()) {
			if($this->save()) {
				return true;
			} else {
				return false;
			}
		} else {
			return $val->error();
		}
	}
	
	/** Edit a link
	 * @param for required parameter, see $_properties
	 */
	public function edit_link($data) {
		$val = Validation::forge();
		$val->add_model($this);
		
		if($val->run()) {
			return $this->set($data)->save();
		} else {
			return $val->error();
		}
	}
	
	/** get all collections
	 * @param
	 */
	public function get_links() {
		$query = DB::select('*')
				->from('links')
				->where('is_show', '1')
				->execute()->as_array();

		return $query;
	}
	
}