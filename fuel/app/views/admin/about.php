	<article class="module width_full">
		<header><h3>About Page</h3></header>
		<?php echo Form::open(array('enctype' => 'multipart/form-data')); ?>
		

		<?php foreach($about as $i => $a): ?>
			<?php if($a->name == 'about_photo'): ?>
				<h3>About Page Photo</h3>
				<?php 
				if(!empty($a->value)):
					echo Html::img('assets/uploads/' . $a->value, array('alt' => '', 'width' => 560, 'height' => 420)); 
				endif; 
				?>
				<fieldset>

				<?php echo Form::label('Upload Image'); ?>
				<p>Please upload image with width:360px, height: 200px!</p>
				<span class="btn btn-file"><?php echo Form::file('about_photo', array()); ?></span>
				</fieldset>
			<?php elseif($a->name == 'about_achievement'): ?>
				<fieldset>
				<?php echo Form::label('About Achievement'); ?>
				<?php echo Form::textarea('about_achievement', $a->value, array('style' => 'width:95%;margin-left:15px; min-height:100px')); ?>
				</fieldset>
			<?php elseif($a->name == 'about_description'): ?>
				<fieldset>
				<?php echo Form::label('About Description'); ?>
				<?php echo Form::textarea('about_description', $a->value, array('style' => 'width:95%;margin-left:15px; min-height:100px')); ?>
				</fieldset>
			<?php endif; ?>

		<?php endforeach; ?>
			
			
			
			
			
			<footer>
				<div class="submit_link">
					<?php echo Form::submit('submit', 'Submit', array('class' => 'btn')); ?>
				</div>
			</footer>
		<?php Form::close(); ?>
	</article>
