<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Welcome extends Controller_Base
{

	/**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		$this->data['links'] = Model_Link::forge()->get_links();
		$this->data['about_description'] = Model_Option::forge()->get_option('about_description');;
		$this->data['about_photo'] = Model_Option::forge()->get_option('about_photo');
		$this->data['contact_address'] = Model_Option::forge()->get_option('contact_address');
		$this->data['contact_city'] = Model_Option::forge()->get_option('contact_city');
		$this->data['contact_country'] = Model_Option::forge()->get_option('contact_country');
		$this->data['contact_phone1'] = Model_Option::forge()->get_option('contact_phone1');
		$this->data['contact_phone2'] = Model_Option::forge()->get_option('contact_phone2');
		$this->data['contact_phone3'] = Model_Option::forge()->get_option('contact_phone3');
		$this->data['contact_fax'] = Model_Option::forge()->get_option('contact_fax');
		$this->data['contact_email'] = Model_Option::forge()->get_option('contact_email');
		$this->data['contact_website'] = Model_Option::forge()->get_option('contact_website');
		$this->data['fb_link'] = Model_Option::forge()->get_option('fb_link');
		$this->data['tw_link'] = Model_Option::forge()->get_option('tw_link');
		$this->data['youtube_link'] = Model_Option::forge()->get_option('youtube_link');
		$this->data['about_achievement'] = Model_Option::forge()->get_option('about_achievement');
		$this->data['insta_link'] = Model_Option::forge()->get_option('insta_link');
		$this->data['collections'] = Model_Collection::forge()->get_collections();
		$this->data['press'] = Model_Press::forge()->get_press();
		$this->data['categories'] = Model_Category::forge()->get_categories(array("type" => "collection", "deleted" => "0"));

		// valhelper::pr($this->data);die;
		$this->main_view = 'welcome/index';
	}

	/**
	 * A typical "Hello, Bob!" type example.  This uses a ViewModel to
	 * show how to use them.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_hello()
	{
		return Response::forge(ViewModel::forge('welcome/hello'));
	}

	/**
	 * The 404 action for the application.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		return Response::forge(ViewModel::forge('welcome/404'), 404);
	}
}
