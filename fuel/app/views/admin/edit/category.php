<article class="module width_full">
	<header><h3>Edit Category</h3></header>
		<?php echo Form::open(); ?>
		<div class="module_content">
			<fieldset>
			<?php echo Form::label('Name'); ?>
			<?php echo Form::input('name', $category->name); ?>
			</fieldset>
			
		</div>
		<footer>
			<div class="submit_link">
				<?php echo Form::submit('submit', 'Save', array('class' => 'btn')); ?>
			</div>
		</footer>
		
		</form>
</article>