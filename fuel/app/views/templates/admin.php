<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>RINALDY A. YUNARDI</title>
<?php echo $css; ?>
</head>
<body>
	<header id="header">
		<hgroup>
			<h2 class="section_title">Admin Panel</h2>
			<?php if(Auth::check()): ?>
				<div class="btn_view_site">
					<a href="<?php echo Uri::create('admin/logout') ?>">Logout</a>
				</div>
			<?php endif; ?>
			<?php //we need to change something here; ?>
			
				
		</hgroup>
	</header>
		<?php if(!empty($success)): ?>
			<div class="alert alert-success">
				<?php echo $success ?>
			</div>
			<?php elseif(!empty($errors)): ?>
			<div class="alert alert-error">
				<?php foreach($errors as $err): ?>
				<div>
					<?php echo $err ?>
				</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	
	<!-- end of header bar -->
	<!-- content start -->
	<?php if(Auth::check()): ?>
	
		<aside id="sidebar" class="column">
			<h3>Content</h3>
			<ul class="toggle">
				<p>General</p>
				<ul class="toggle">
					<li><i class="icon-th-list"></i><?php echo Html::anchor(Uri::create('admin/collection_categories'), 'All Collection Categories'); ?></li>
					<li><i class="icon-th-list"></i><?php echo Html::anchor(Uri::create('admin/press_categories'), 'All Press Categories'); ?></li>
					<li><i class="icon-plus"></i><?php echo Html::anchor(Uri::create('admin/add_category/category'), "Add Category"); ?></li>
				</ul>

				<p>Collections</p>
				<ul class="toggle">
					<li><i class="icon-th-list"></i><?php echo Html::anchor(Uri::create('admin/collections_lists'), 'All Collections'); ?></li>
					<li><i class="icon-plus"></i><?php echo Html::anchor(Uri::create('admin/add_collection'), 'Add Collection'); ?></li>
				</ul>

				<p>Press</p>
				<ul class="toggle">
					<li><i class="icon-th-list"></i><?php echo Html::anchor(Uri::create('admin/press_lists'), 'All Presses'); ?></li>
					<li><i class="icon-plus"></i><?php echo Html::anchor(Uri::create('admin/add_press'), 'Add Press'); ?></li>
				</ul>

				<p>About Page</p>
				<ul class="toggle">
					<li><i class="icon-th-list"></i><?php echo Html::anchor(Uri::create('admin/about'), 'About Page'); ?></li>
				</ul>

				<p>Contact Page</p>
				<ul class="toggle">
					<li><i class="icon-th-list"></i><?php echo Html::anchor(Uri::create('admin/links_lists'), 'Contact Page Links'); ?></li>
					<li><i class="icon-th-list"></i><?php echo Html::anchor(Uri::create('admin/options'), 'Contact Page Options'); ?></li>
				</ul>

			</ul>
	
			<footer>
				<hr />
				<p>
					<strong>Copyright &copy; 2013 RINALDY A. YUNARDI</strong>
				</p>
			</footer>
		</aside>
		<!-- end of sidebar -->
		<?php endif; ?>
		<section id="main" class="column">
			<?php echo $main_view; ?>
		</section>
	<!-- content end -->
	<?php echo $js; ?>
</body>
</html>
