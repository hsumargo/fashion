<article class="module width_full">
	<header>
		<h3 class="tabs_involved">Collection's Categories</h3>
	</header>

	<div class="tab_container">
		<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0">
				<thead>
					<tr>
						<th>Category Name</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($categories as $category):?>
						<tr>
							<td><?php echo ucfirst($category['name']); ?></td>
							<td>
								<small> <?php echo Html::anchor(Uri::create('admin/edit_category/' . $category['id']), '<i class="icon-edit"></i> Edit Category', array('class' => 'btn btn-mini')); ?></small>
								<small> <?php echo Html::anchor(Uri::create('admin/delete/category/' . $category['id']), '<i class="icon-trash"></i> Delete Category', array('class' => 'btn btn-mini btn-danger')); ?></small>
							</td>
						</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		</div>