<article class="module width_full">
	<header><h3>More Image <?php echo $type; ?></h3></header>
	<?php echo Form::open(array('enctype' => 'multipart/form-data')); ?>
	
		<?php 
		if(!empty($more->name)):
			echo Html::img('assets/uploads/' . $more->name, array('alt' => '', 'width' => 350, 'height' => 130)); 
		endif; 
		?>
		<fieldset>
		<?php echo Form::label('Upload Image'); ?>
		<p>Please upload image with width:350px, height: 130px!</p>
		<span class="btn btn-file"><?php echo Form::file('name', array()); ?></span>
		<?php echo Form::hidden('slide', ($type == 'before') ? '12' : '11'); ?>
		</fieldset>

		<footer>
			<div class="submit_link">
				<?php echo Form::submit('submit', 'Submit', array('class' => 'btn')); ?>
			</div>
		</footer>
	<?php Form::close(); ?>
</article>
