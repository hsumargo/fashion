<?php
/**
 * The development database settings.
 */
if($_SERVER['HTTP_HOST'] == 'fashion') {
	return array(
		'default' => array(
			'type' => 'mysql',
			'connection'  => array(
				'username'   => 'root',
				'password'   => '',
				'database' => 'fashion',
				'persistent' => false
			),
		),
	);

} else {
	return array(
		'default' => array(
			'type' => 'mysql',
			'connection'  => array(
				'username'   => 'root',
				'password'   => 'qwerty',
				'database' => 'fashion',
				'persistent' => false
			),
		),
	);
}