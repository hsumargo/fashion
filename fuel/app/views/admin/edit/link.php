<article class="module width_full">
	<header><h3>Edit Link</h3></header>
	<?php //Form::set_values($post);?>
		<?php echo Form::open(); ?>
		<div class="module_content">
			<fieldset>
			<?php echo Form::label('Person Name'); ?>
			<?php echo Form::input('name', $link->name); ?>
			</fieldset>

			<fieldset>
			<?php echo Form::label('Link'); ?>
			<?php echo Form::input('link', $link->link); ?>
			</fieldset>
			
			<fieldset>
			<?php echo Form::label('Show'); ?>
			<?php echo Form::select('is_show', $link->is_show, array('1' => 'Yes', '0' => 'No'), array('class' => 'input-small')); ?>
			</fieldset>

			
		</div>
		<footer>
			<div class="submit_link">
				<?php echo Form::submit('submit', 'Save Link', array('class' => 'btn')); ?>
			</div>
		</footer>
		
		</form>
</article>