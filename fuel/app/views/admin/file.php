<?php for($i=1; $i<=5; $i++): ?>
	<article class="module width_full">
		<?php if($i == 1):?>
		<header><h3>Product Main Photo</h3></header>
		<?php elseif($i == 5): ?>
		<header><h3>Product Thumbnail Photo</h3></header>
		<?php else: ?>
		<header><h3>Product Photo <?php echo $i-1; ?></h3></header>
		<?php endif; ?>
		<?php 
			if(!empty($files[$i])):
				echo Html::img('assets/uploads/' . $files[$i]->name, array('alt' => '', 'width' => 380, 'height' => 270)); 
			endif; 
		?>
		<?php echo Form::open(array('enctype' => 'multipart/form-data')); ?>
			<fieldset>
			<?php echo Form::label('Upload Image'); ?>
			<p>Please upload image with width:380px, height: 270px!</p>
			<span class="btn btn-file"><?php echo Form::file('name', array()); ?></span>
			<?php echo Form::hidden('product_id', $product_id); ?>
			<?php echo Form::hidden('order', $i); ?>
			</fieldset>
			
			<footer>
				<div class="submit_link">
					<?php echo Form::submit('submit', 'Submit', array('class' => 'btn')); ?>
				</div>
			</footer>
		<?php echo Form::close(); ?>
	</article>
	<?php endfor; ?>
	
	
