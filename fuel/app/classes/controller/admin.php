<?php
class Controller_Admin extends Controller_Base {
	protected $user;

	/** Run this function before go to action controller
	 */
	public function before() {
		parent::before();
		$user = Auth::check();

		//set css and js to admin style
		$this->data['admin'] = true;
		$auth = Auth::instance();
		if($this->request->route->action != 'login') {
			if(!$user) {
				Response::redirect('/admin/login');
			}
			$this->user = Auth::instance()->get_screen_name();
		}
	}
	
	/** Routes
	 */
	public function action_index() {
		$auth = Auth::instance();
		Response::redirect('admin/login');
	}
	
	/** Login page Admin
	 * @return boolean
	 */
	public function action_login() {
		if (Input::post()) {
			$auth = Auth::instance();
			if ($auth->login()) {
				Response::redirect('admin/home');
			} else {
				$this->data['errors'] = array('Wrong username/password combo. Try again');
			}
		}
		
		$this->data['nav'] = false;
		// Show the login form
		$this->main_view = 'admin/login';
	}
	
	/** Admin logout
	 * @return boolean
	 */
	public function action_logout() {
		Auth::instance()->logout();
		$this->data['success'] = 'Success Logout';
		Response::redirect('admin/login');
	}
	
	/** Home page admin panel (need to login first)
	 */
	public function action_home() {
		$this->main_view = 'admin/home';
	}

	/*Get List of Collections*/
	public function action_collections_lists() {
		$this->data['collections'] = Model_Collection::forge()->get_collections(true);
		$this->main_view = 'admin/list/collection';
	}

	/*Get List of Collections*/
	public function action_collection_categories() {
		$this->data['categories'] = Model_Category::find('all', array("where" => array("type" => "collection", "deleted" => "0")));
		$this->main_view = 'admin/list/category';
	}

	/*Get List of Collections*/
	public function action_press_categories() {
		$this->data['categories'] = Model_Category::find('all', array("where" => array("type" => "press", "deleted" => "0")));
		$this->main_view = 'admin/list/category';
	}

	/* Get List of Press Release*/
	public function action_press_lists() {
		$this->data['press'] = Model_Press::forge()->get_press();
		$this->main_view = 'admin/list/press';
	}

	/* Get List of Links*/
	public function action_links_lists() {
		$this->data['links'] = Model_Link::forge()->get_links();
		$this->main_view = 'admin/list/link';
	}

	/** Add a collection
	 */
	public function action_add_collection() {
		if(Input::method() == 'POST') {
			$post = Input::post();

			if(empty($_FILES['filename']['name'])) {
				$this->data['errors'] = array('Please provide name, file, and category!');
			} else {
				$add = Model_Collection::forge($post)->add_collection();

				if($add !== false) {
					// Custom configuration for this upload
					$config = array(
							'path' => 'assets/uploads',
							'randomize' => true,
							'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
					);
						
					// process the uploaded files in $_FILES
					Upload::process($config);
						
					// if there are any valid files
					if (Upload::is_valid()) {
						// save them according to the config
						Upload::save();
						//get data after upload
						$upload_file = Upload::get_files();
						//save filename to database
						$upload = Model_Collection::find($add)->edit_collection(array('filename' => $upload_file[0]['saved_as']));
						
						if($upload === true) {
							$this->data['success'] = 'Collection added successfully';
						}
					}
				} else {
					$this->data['errors'] = array('Please provide name, file, and category!');
				}
			}
		}

		$categories = Model_Category::find('all', array("where" => array("type" => "collection", "deleted" => "0")));
		$result = array();
		foreach ($categories as $i => $obj)
		{
		    $result[$obj->id] = ucfirst($obj->name);
		}

		$this->data['categories'] = $result;
	
		$this->main_view = 'admin/add/collection';
	}

	/** Add a collection's category
	 */
	public function action_add_category() {
		if(Input::method() == 'POST') {
			$post = Input::post();

			$add = Model_Category::forge($post)->add_category();

			if($add === true) {
				$this->data['success'] = 'Category added successfully';
			} else {
				$this->data['errors'] = array('Please provide name!');
			}
		}
		$this->main_view = 'admin/add/category';
	}

	/** Edit Category
	 * @param int $category_id
	 */
	public function action_edit_category($category_id = null) {
		if(Input::method() == 'POST') {
			$post = Input::post();
			unset($post['submit']);

			$edit = Model_Category::find($category_id)->edit_category($post);
			if($edit === true) {
				$this->data['success'] = 'Category edited successfully';
			} else {
				$this->data['errors'] = array("Please give valid name!");
			}
		}

		$this->data["category"] = Model_Category::find($category_id);

		$this->main_view = 'admin/edit/category';
	}

	/** Edit Collection
	 * @param int $collection_id
	 */
	public function action_edit_collection($collection_id = null) {
		if(Input::method() == 'POST') {
			$post = Input::post();
			unset($post['submit']);

			$edit = Model_Collection::find($collection_id)->edit_collection($post);
		
			if(!empty($_FILES)) {
				// Custom configuration for this upload
				$config = array(
						'path' => 'assets/uploads',
						'randomize' => true,
						'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
				);
					
				// process the uploaded files in $_FILES
				Upload::process($config);
					
				// if there are any valid files
				if (Upload::is_valid()) {
					// save them according to the config
					Upload::save();
					//get data after upload
					$upload_file = Upload::get_files();
					//save filename to database
					$edit = Model_Collection::find($collection_id)->edit_collection(array('filename' => $upload_file[0]['saved_as']));
				}
			}
	
			if($edit === true) {
				$this->data['success'] = 'Collection edited successfully';
			} else {
				$this->data['errors'] = $edit;
			}
		}

		$collection = Model_Collection::find($collection_id);
		
		$categories = Model_Category::find('all', array("where" => array("type" => "collection", "deleted" => "0")));
		$result = array();
		foreach ($categories as $i => $obj)
		{
		    $result[$obj->id] = ucfirst($obj->name);
		}

		$this->data['categories'] = $result;
		$this->data['collection'] = $collection;

		$this->main_view = 'admin/edit/collection';
	}

	/** Add a press
	 */
	public function action_add_press() {
		if(Input::method() == 'POST') {
			$post = Input::post();
			
			if(empty($_FILES['filename']['name']) || empty($post['name'])) {
				$this->data['errors'] = array('Please provide name and file!');
			} else {
				$add = Model_Press::forge($post)->add_press();

				if($add !== false) {
					// Custom configuration for this upload
					$config = array(
							'path' => 'assets/uploads',
							'randomize' => true,
							'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
					);
						
					// process the uploaded files in $_FILES
					Upload::process($config);
						
					// if there are any valid files
					if (Upload::is_valid()) {
						// save them according to the config
						Upload::save();
						//get data after upload
						$upload_file = Upload::get_files();
						//save filename to database
						$upload = Model_Press::find($add)->edit_press(array('filename' => $upload_file[0]['saved_as']));
						
						if($upload === true) {
							$this->data['success'] = 'Press added successfully';
						}
					}
				} else {
					$this->data['errors'] = array('Please provide name and file!');
				}
			}
		}

		$categories = Model_Category::find('all', array("where" => array("type" => "press", "deleted" => "0")));
		foreach ($categories as $i => $obj)
		{
		    $result[$obj->id] = ucfirst($obj->name);
		}

		$this->data['categories'] = $result;

		$this->main_view = 'admin/add/press';
	}

	/** Edit Press
	 * @param int $press_id
	 */
	public function action_edit_press($press_id = null) {
		if(Input::method() == 'POST') {
			$post = Input::post();
			unset($post['submit']);

			$edit = Model_Press::find($press_id)->edit_press($post);
		
			if(!empty($_FILES)) {
				// Custom configuration for this upload
				$config = array(
						'path' => 'assets/uploads',
						'randomize' => true,
						'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
				);
					
				// process the uploaded files in $_FILES
				Upload::process($config);
					
				// if there are any valid files
				if (Upload::is_valid()) {
					// save them according to the config
					Upload::save();
					//get data after upload
					$upload_file = Upload::get_files();
					//save filename to database
					$edit = Model_Press::find($press_id)->edit_press(array('filename' => $upload_file[0]['saved_as']));
				}
			}
			
			if($edit === true) {
				$this->data['success'] = 'Post edited successfully';
			} else {
				$this->data['errors'] = $edit;
			}
		}

		$categories = Model_Category::find('all', array("where" => array("type" => "press", "deleted" => "0")));
		$result = array();
		foreach ($categories as $i => $obj)
		{
		    $result[$obj->id] = ucfirst($obj->name);
		}

		$this->data['categories'] = $result;

		$press = Model_Press::find($press_id);
		
		$this->data['press'] = $press;

		$this->main_view = 'admin/edit/press';
	}

	/** Edit Press
	 * @param int $press_id
	 */
	public function action_edit_link($link_id = null) {
		if(Input::method() == 'POST') {
			$post = Input::post();
			unset($post['submit']);

			$edit = Model_Link::find($link_id)->edit_link($post);
	
			if($edit === true) {
				$this->data['success'] = 'Link edited successfully';
			} else {
				$this->data['errors'] = $edit;
			}
		}

		$link = Model_Link::find($link_id);

		$this->data['link'] = $link;

		$this->main_view = 'admin/edit/link';
	}

	/** Edit Collection
	 */
	public function action_about() {
		if(Input::method() == 'POST') {
			$post = Input::post();
			unset($post['submit']);

			if(!empty($post['about_description'])) {
				$desc = Model_Option::find(1);
				$desc->value = $post['about_description'];
				$desc->save();
			}
			if(!empty($post['about_achievement'])) {
				$achieve = Model_Option::find(16);
				$achieve->value = $post['about_achievement'];
				$achieve->save();
			}

			if(!empty($_FILES['about_photo']['name'])) {
				// Custom configuration for this upload
				$config = array(
						'path' => 'assets/uploads',
						'randomize' => true,
						'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
				);
					
				// process the uploaded files in $_FILES
				Upload::process($config);
					
				// if there are any valid files
				if (Upload::is_valid()) {
					// save them according to the config
					Upload::save();
					//get data after upload
					$upload_file = Upload::get_files();
					//save filename to database
					$upload = Model_Option::find(2);
					$upload->value = $upload_file[0]['saved_as'];
					$upload->save();
				}
			} else {
				$this->data['errors'] = array('photo did not change');
			}
			
			
			$this->data['success'] = 'About Page edited successfully';
		}

		$this->data['about'] = Model_Option::find('all', array('where' => array('autoload' => '0')));

		$this->main_view = 'admin/about';
	}
	
	/** Delete a post
	 * @param string $type, int $post_id
	 */
	public function action_delete($type = null, $post_id = null) {

		if(!empty($type) && !empty($post_id)) {
			if($type == 'collections') {
				$delete = Model_Collection::find($post_id);
			} elseif($type == 'press') {
				$delete = Model_Press::find($post_id);
			} elseif($type == "category") {
				$delete = Model_Category::find($post_id);
			}
			$delete->deleted = 1;
			$delete->save();
			if($type == "category") {
				Response::redirect('/admin/home');	
			}
			Response::redirect('/admin/'.$type.'_lists');
		} else {
			Response::redirect('/admin/home');
		}
	}
	
	
	/** Options
	 */
	public function action_options() {
		if(Input::method() == 'POST') {
			$post = Input::post();
			unset($post['submit']);
			foreach($post as $key => $value) {
				$edit = Model_Option::find('first', array('where' => array('name' => $key, 'autoload' => '1')));
				$edit->value = $value;
				$edit->save();
			}
			$this->data['success'] = 'Options edited successfully';
		}
		$options = Model_Option::find('all', array('where' => array('autoload' => '1')));
		
		$this->data['options'] = $options;
		$this->main_view = 'admin/options';
	}
	
	
	/** Page not found
	 * @return boolean
	 */
	public function action_404()
	{
		$messages = array('Aw!', 'Uh Oh!', 'Nope, not here.', 'Huh?');
		$data['title'] = $messages[array_rand($messages)];
	
		// Set a HTTP 404 output header
		return Response::forge(View::forge('welcome/404', $data), 404);
	}
}
