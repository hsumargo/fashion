<article class="module width_full">
	<header><h3>Edit Press</h3></header>
	<?php //Form::set_values($post);?>
		<?php echo Form::open(array('enctype' => 'multipart/form-data')); ?>
		<div class="module_content">
			<fieldset>
			<?php echo Form::label('Name'); ?>
			<?php echo Form::input('name', $press->name); ?>
			</fieldset>
			
			<?php 
			if(!empty($press->filename)):
				echo Html::img('assets/uploads/' . $press->filename, array('alt' => '', 'width' => 560, 'height' => 420)); 
			endif; 
			?>
			
			<?php echo Form::label('Upload Image'); ?>
			<span class="btn btn-file"><?php echo Form::file('filename', array()); ?></span>
			</fieldset>

			<fieldset>
			<?php echo Form::label('Category'); ?>
			<?php echo Form::select('category_id', $press->category_id, $categories); ?>
			</fieldset>

		</div>
		<footer>
			<div class="submit_link">
				<?php echo Form::submit('submit', 'Save Press', array('class' => 'btn')); ?>
			</div>
		</footer>
		
		</form>
</article>