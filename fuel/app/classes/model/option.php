<?php 
class Model_Option extends Model_Base {
	static $_properties = array(
		'id', 
		'name' => array('label' => 'Name', 'validation' => array('required')),
		'value'  => array('label' => 'Value', 'validation' => array('required')),
		'autoload', 'label'
	);
	
	/** Edit options
	 * @param for required parameter, see $_properties
	 */
	public function edit_options($data) {
		$val = Validation::forge();
		$val->add_model($this);
		
		if($val->run()) {
			return $this->set($data)->save();
		} else {
			return $val->error();
		}
	}
	
	/** get option by name
	 * @param for required parameter, see $_properties
	 */
	public function get_option($options_name = null) {
		$query = DB::select('name', 'value')->from('options')
					->where('name', '=', $options_name);
		
		$result = $query->execute()->as_array();

		foreach($result as $r) {
			foreach($r as $idx => $hsl) {
				$results = $hsl;
			}
		}
		return $results;
	}
}