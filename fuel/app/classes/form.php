<?php
class Form extends \Fuel\Core\Form {
	static $values = array();

	public static function set_values($v) {
		Form::$values = $v;
	}

	public static function input($field, $value = NULL, array $attributes = array()) {
		if(!empty(Form::$values[$field]) && empty($value)) {
			$value = Form::$values[$field];
		}

		return parent::input($field, $value, $attributes);
	}

	public static function textarea($field, $value = null, array $attributes = array()) {
		if(!empty(Form::$values[$field]) && empty($value)) {
			$value = Form::$values[$field];
		}
	
		return parent::textarea($field, $value, $attributes);
	}

	public static function radio($field, $value = NULL, $checked = null, array $attributes = array()) {
		if(!empty(Form::$values[$field]) && $value == Form::$values[$field]) {
			$checked = true;
		}
	
		return parent::radio($field, $value, $checked, $attributes);
	}
	
	//public static function select($field, $values = null, array $options = array(), array $attributes = array())
	public static function select($field, $values = null, array $options = array(), array $attributes = array()) {
		// birthdate select
		if($field == 'birthdate') {
			$fields = array('birthday', 'birthmth', 'birthyr');
			foreach($fields as $f) {
				if(empty(Form::$values[$f])) {
					Form::$values[$f] = '';
				}
			}
	
			$attributes = array_merge(array(
					'between' => '', 'month_type' => 'en-sh'
			), $attributes);
	
			$between = $attributes['between'];
	
			$ret =
			Form::select('birthday', range(1,31)) . $between .
			Form::select('birthmth', HGB::$months[$attributes['month_type']]) . $between .
			Form::select('birthyr', array_combine(range(date('Y'), 1900, -1), range(date('Y'), 1900, -1)));
	
			unset($attributes['between']); unset($attributes['month_type']);
			return $ret;
	
		}
// 		 elseif(!empty(Form::$values[$field]) && empty($values)) {
// 			$values = Form::$values[$field];
// 		}
	
		return parent::select($field, $values, $options, $attributes);
	}
}