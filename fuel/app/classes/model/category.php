<?php 
class Model_Category extends Model_Base {
	static $_properties = array(
		'id', 
		'name' => array('label' => 'Name', 'validation' => array('required')),
		'orders' => array('label' => 'Orders'),
		'type',
		'deleted'
		
	);

	/** Add a categroy
	 * @param for required parameter, see $_properties
	 */
	public function add_category() {
		$val = Validation::forge();
		$val->add_model($this);

		if($val->run()) {
			
			$query = DB::query("SELECT orders FROM categories 
								WHERE type = '".$this->type."'
								ORDER BY orders DESC
								LIMIT 1")
					->execute()->as_array();

			$this->orders = $query[0]['orders'] + 1;
			$this->deleted = 0;

			return $this->save();
		} else {
			return $val->show_errors();
		}
	}
	
	/** Edit a category
	 * @param for required parameter, see $_properties
	 */
	public function edit_category($data) {
		$val = Validation::forge();
		$val->add_model($this);

		if($val->run()) {
			return $this->set($data)->save();
		} else {
			return $val->error();
		}
	}

	public function get_categories($option = array()) {
		$query = DB::select('name')
				->from('categories')
				->where("type", $option["type"]);

		if(isset($option['deleted'])) {
			$query->where("deleted", "0");
		}
					
		$result = $query->execute()->as_array();

		foreach($result as $r) {
			foreach($r as $idx => $hsl) {
				$results[] = $hsl;
			}
		}
		return $results;
	}
}