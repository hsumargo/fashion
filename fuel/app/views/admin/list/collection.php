<article class="module width_full">
	<header>
		<h3 class="tabs_involved">Collections Content Manager</h3>
	</header>

	<div class="tab_container">
		<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0">
				<thead>
					<tr>
						<th>Collection ID</th>
						<th>Category Name</th>
						<th>Collection Name</th>
						<th>Created On</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($collections as $collection):?>
						<tr>
							<td><?php echo $collection['id']; ?></td>
							<td><?php echo ucfirst($collection['category_name']); ?></td>
							<td><?php echo ucfirst($collection['name']); ?></td>
							<td><?php echo (!empty($collection['created_at'])) ? date('d M Y', $collection['created_at']) : ''; ?></td>
							<td>
								<small> <?php echo Html::anchor(Uri::create('admin/edit_collection/' . $collection['id']), '<i class="icon-edit"></i> Edit Collection', array('class' => 'btn btn-mini')); ?></small>
								<small> <?php echo Html::anchor(Uri::create('admin/delete/collections/' . $collection['id']), '<i class="icon-trash"></i> Delete Collection', array('class' => 'btn btn-mini btn-danger')); ?></small>
							</td>
						</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		</div>