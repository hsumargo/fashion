<article class="module width_full">
	<header><h3>Add Collection Category</h3></header>
		<?php echo Form::open(); ?>
		<div class="module_content">
			<fieldset>
			<?php echo Form::label('Name'); ?>
			<?php echo Form::input('name'); ?>
			</fieldset>
			
			<?php echo Form::hidden("type", "collection"); ?>

		</div>
		<footer>
			<div class="submit_link">
				<?php echo Form::submit('submit', "Save", array('class' => 'btn')); ?>
			</div>
		</footer>
		
		</form>
</article>

<article class="module width_full">
	<header><h3>Add Press Category</h3></header>
		<?php echo Form::open(); ?>
		<div class="module_content">
			<fieldset>
			<?php echo Form::label('Name'); ?>
			<?php echo Form::input('name'); ?>
			</fieldset>
			
			<?php echo Form::hidden("type", "press"); ?>

		</div>
		<footer>
			<div class="submit_link">
				<?php echo Form::submit('submit', "Save", array('class' => 'btn')); ?>
			</div>
		</footer>
		
		</form>
</article>