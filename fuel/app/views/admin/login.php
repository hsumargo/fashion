
	<div class="login-form">
	    <div class="thumbnail thumbnail-login">
		<?php echo Form::open(array('id' => 'login-form', 'style' => 'padding:10px;')) ?>
		<h3><?php echo (!empty($admin_login)) ? 'Admin' : 'User' ?> Login</h3>
		<?php echo Form::input('username', null, array('placeholder' => 'Username')) ?>
		<br>
		<?php echo Form::password('password', null, array('placeholder' => 'Password')) ?>
		
		<br><br>
		
		<?php echo Form::submit('submit', 'Login', array('class' => 'btn pull-left')) ?>
		<?php echo Form::close() ?>
		</div>
	</div>