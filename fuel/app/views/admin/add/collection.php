<article class="module width_full">
	<header><h3>Add Collection</h3></header>
		<?php echo Form::open(array('enctype' => 'multipart/form-data')); ?>
		<div class="module_content">
			<fieldset>
			<?php echo Form::label('Name'); ?>
			<?php echo Form::input('name'); ?>
			</fieldset>
			
			<?php echo Form::label('Upload Image'); ?>
			<p>Please upload image with width:280px, height: 210px!</p>
			<span class="btn btn-file"><?php echo Form::file('filename', array()); ?></span>
			</fieldset>

			<fieldset>
			<?php echo Form::label('Category'); ?>
			<?php echo Form::select('category_id',null, $categories); ?>
			</fieldset>

			
		</div>
		<footer>
			<div class="submit_link">
				<?php echo Form::submit('submit', 'Save Collection', array('class' => 'btn')); ?>
			</div>
		</footer>
		
		</form>
</article>