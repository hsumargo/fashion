<article class="module width_full">
	<header>
		<h3 class="tabs_involved">Links Content Manager</h3>
	</header>

	<div class="tab_container">
		<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0">
				<thead>
					<tr>
						<th>Link ID</th>
						<th>Person</th>
						<th>Link</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($links as $p):?>
						<tr>
							<td><?php echo $p['id']; ?></td>
							<td><?php echo ucfirst($p['name']); ?></td>
							<td><?php echo ucfirst($p['link']); ?></td>
							<td>
								<small> <?php echo Html::anchor(Uri::create('admin/edit_link/' . $p['id']), '<i class="icon-edit"></i> Edit Link', array('class' => 'btn btn-mini')); ?></small>
								
							</td>
						</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		</div>