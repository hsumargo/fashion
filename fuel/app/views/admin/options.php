<article class="module width_full">
	<header><h3>Edit General Website Options</h3></header>
	<?php Form::set_values($options); ?>
	<form method="post">
	<div class="module_content">
		<?php foreach($options as $i => $option):?>
			<fieldset>
			<?php echo Form::label($option->label); ?>
			<?php echo Form::input($option->name, $option->value); ?>
			</fieldset>
		<?php endforeach; ?>
		<footer>
			<div class="submit_link">
				<?php echo Form::submit('submit', 'Save', array('class' => 'btn')); ?>
			</div>
		</footer>
	</div>
	</form>
</article>