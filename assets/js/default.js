var canvas = document.getElementById('line1');
var context = canvas.getContext('2d');
context.beginPath();
context.moveTo(200, 0);
context.lineTo(1800, 0);
context.lineTo(2000, 150);
context.lineTo(2000, 850);
context.lineTo(1800, 1000);
context.lineTo(200, 1000);
context.lineTo(0, 850);
context.lineTo(0, 150);
context.lineTo(200, 0);
context.lineTo(300, 120);
context.lineTo(330, 280);
context.lineTo(150, 230);
context.lineTo(0, 150);
context.moveTo(300, 120);
context.lineTo(1700, 120);
context.lineTo(1850, 230);
context.lineTo(1850, 770);
context.lineTo(1700, 880);
context.lineTo(300, 880);
context.lineTo(150, 770);
context.lineTo(150, 230);
context.lineTo(300, 120);
context.moveTo(1800, 0);
context.lineTo(1700, 120);
context.lineTo(1670, 280);
context.lineTo(1850, 230);
context.lineTo(2000, 150);
context.moveTo(2000, 850);
context.lineTo(1850, 770);
context.lineTo(1670, 720);
context.lineTo(1700, 880);
context.lineTo(1800, 1000);
context.moveTo(200, 1000);
context.lineTo(300, 880);
context.lineTo(330, 720);
context.lineTo(150, 770);
context.lineTo(0, 850);
context.moveTo(150, 230);
context.lineTo(330, 720);
context.lineTo(1700, 880);
context.moveTo(150, 770);
context.lineTo(330, 280);
context.lineTo(1700, 120);
context.moveTo(1850, 230);
context.lineTo(1670, 720);
context.lineTo(300, 880);
context.moveTo(300, 120);
context.lineTo(1670, 280);
context.lineTo(1850, 770);
context.lineJoin = 'miter';
context.strokeStyle = '#86754d';
context.stroke();

function mouse() {
	$('.canvaslink').on('mousemove', function(e){
		if(e.pageX > getScreenCenterX() && e.pageY > getScreenCenterY()) {
			$('#canvas-wrapper').attr('class', 'canvas-wrapper press');
		} else if (e.pageX > getScreenCenterX() && e.pageY < getScreenCenterY()) {
			$('#canvas-wrapper').attr('class', 'canvas-wrapper contact');
		} else if (e.pageX < getScreenCenterX() && e.pageY < getScreenCenterY()) {
			$('#canvas-wrapper').attr('class', 'canvas-wrapper about');
		} else if (e.pageX < getScreenCenterX() && e.pageY > getScreenCenterY()) {
			$('#canvas-wrapper').attr('class', 'canvas-wrapper collection');
		}
	});
	$('.link1, .link2, .link3, .link4').on('click', function() {
		$('#logo').hide();
		$('#logo2').attr('class', 'logofix active gone');
		$('#logo3').attr('class', 'logofix2 active');
		$('#logo3').css('z-index', '10');
		$('#canvas-wrapper').attr('class', 'canvas-wrapper active');
		$("#aboutus, #contact, #collection, #collection-detail, #press, .co-details").css('display', 'none');
		$('#' + $(this).data('id')).fadeIn("slow").css('display', 'block');
		$('.canvaslink').unbind('mousemove');
		$(".nano").nanoScroller();
	});
}
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
	$('#logo').on('click', function() {
		$(".logo-arrow").animate({
		    top: "0px",
		    opacity: 1
		}, 300 );
		$(".logo-bow").animate({
		    top: "63px",
		    opacity: 1
		}, 300 );
		$(".logo-leftwing").animate({
		    opacity: 1,
		    left: "27px",
		    top: "40px",
		}, {
			step: function(now,fx) {
			      $(this).css('-webkit-transform','rotate(0deg)');
		    },
		    duration:'fast'
		}, 'linear' );
		$(".logo-rightwing").animate({
		    opacity: 1,
		    right: "27px",
		    top: "40px",
		    
		}, {
			step: function(now,fx) {
			      $(this).css('-webkit-transform','rotate(0deg)');
		    },
		    duration:'fast'
		}, 'linear' );
		$('#canvas-wrapper').attr('class', 'canvas-wrapper active');
		$('#logo').hide();
		$('#logo2').attr('class', 'logofix active');
		$('.logo-bow').attr('class', 'logo-arrow active');
		$('.logo-leftwing').attr('class', 'logo-bow active');
		$('.logo-rightwing').attr('class', 'logo-leftwing active');
		$('.logo-diamond').attr('class', 'logo-rightwing');
		mouse();
	});
} else {
	$('#logo').on('click', function() {
		$('#canvas-wrapper').attr('class', 'canvas-wrapper active');
		$('#logo').hide();
		$('#logo2').attr('class', 'logofix active');
		$('.logo-bow').attr('class', 'logo-arrow active');
		$('.logo-leftwing').attr('class', 'logo-bow active');
		$('.logo-rightwing').attr('class', 'logo-leftwing active');
		$('.logo-diamond').attr('class', 'logo-rightwing');
		mouse();
	});
}
$('#collection ul li a').click(function() {
	$('#collection').fadeOut('slow');
	var id = $(this).data('id');
	console.log(id);
	$('#collection-' + id).fadeIn('slow');
});

$('#logo3').on('click', function(e) {
	$("#aboutus, #contact, #collection, #press, #collection-detail, .co-details").fadeOut("slow");
	$(this).attr('class', 'logofix2');
	$('#logo2').attr('class', 'logofix active');
	mouse();
});

function getScreenCenterY() {
	var y = 0;
	y = getScrollOffset()+(getInnerHeight()/2);
	return(y);
}
 
function getScreenCenterX() {
	return(document.body.clientWidth/2);
}

function getScreenCenterX() {
	return(document.body.clientWidth/2);
}
	 
function getInnerHeight() {
	var y;
	if (self.innerHeight) {// all except Explorer
		y = self.innerHeight;
	} else if (document.documentElement && sdocument.documentElement.clientHeight) {
		// Explorer 6 Strict Mode
		y = document.documentElement.clientHeight;
	} else if (document.body) {// other Explorers
		y = document.body.clientHeight;
	}
	return(y);
}
	 
function getScrollOffset() {
	var y;
	if (self.pageYOffset) {// all except Explorer
		y = self.pageYOffset;
	} else if (document.documentElement && document.documentElement.scrollTop) {
		// Explorer 6 Strict
		y = document.documentElement.scrollTop;
	} else if (document.body) {// all other Explorers
		y = document.body.scrollTop;
	}
	return(y);
}

$(function() {
	$('.banner').unslider({
		arrows: true,
		delay: 100000000000000,
	});
	$(".nano").nanoScroller({ alwaysVisible: true });
});